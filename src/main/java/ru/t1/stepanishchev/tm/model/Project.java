package ru.t1.stepanishchev.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.stepanishchev.tm.api.model.IWBS;
import ru.t1.stepanishchev.tm.enumerated.Status;

import java.util.Date;

@NoArgsConstructor
@Getter
@Setter
public final class Project extends AbstractUserOwnedModel implements IWBS {

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private Status status = Status.NOT_STARTED;

    @NotNull
    private Date created = new Date();

    public Project(@NotNull final String name, @NotNull final Status status) {
        this.name = name;
        this.status = status;
    }

    @Override
    @NotNull
    public String toString() {
        return name + " : " + description;
    }

}