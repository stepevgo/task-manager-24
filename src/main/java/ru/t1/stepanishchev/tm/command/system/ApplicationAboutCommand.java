package ru.t1.stepanishchev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    private final String NAME = "about";

    @NotNull
    private final String ARGUMENT = "-a";

    @NotNull
    private final String DESCRIPTION = "Show info about developer.";

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("name: Evgeniy Stepanishchev");
        System.out.println("e-mail: estepanischev@t1-consulting.ru");
        System.out.println("e-mail: stepevgo.vrn@gmail.com");
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}