package ru.t1.stepanishchev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.enumerated.ProjectSort;
import ru.t1.stepanishchev.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    @Nullable
    M add(@Nullable String userId, @NotNull M model);

    @NotNull
    List<M> findAll(@NotNull final String userId);

    @NotNull
    List<M> findAll(@Nullable final String userId, @NotNull final Comparator<M> comparator);

    @NotNull
    List<M> findAll(@Nullable final String userId, @Nullable final ProjectSort sort);

    @NotNull
    M findOneByIndex(@NotNull final String userId, @NotNull final Integer index);

    @Nullable
    M findOneById(@Nullable final String userId, @Nullable final String id);

    boolean existsById(@NotNull final String userId, @NotNull final String id);

    @Nullable
    M removeOne (@Nullable final String userId, @Nullable final M model);

    @NotNull
    M removeOneByIndex(@NotNull final String userId, @NotNull final Integer index);

    @Nullable
    M removeOneById(@Nullable final String userId, @Nullable final String id);

    void removeAll(@NotNull final String userId);

    int getSize(@NotNull final String userId);

}