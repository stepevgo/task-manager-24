package ru.t1.stepanishchev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.enumerated.Role;
import ru.t1.stepanishchev.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    User create(@NotNull final String login, @NotNull final String password);

    @NotNull
    User create(@NotNull final String login, @NotNull final String password, @NotNull final String email);

    @NotNull
    User create(@NotNull final String login, @NotNull final String password, @NotNull final Role role);

    @Nullable
    User findByLogin(@NotNull final String login);

    @Nullable
    User findByEmail(@NotNull final String email);

    @NotNull
    Boolean isLoginExist(@NotNull final String login);

    @NotNull
    Boolean isEmailExist(@NotNull final String email);

}